<?php
//session_start();
include('includes/config.php');
include('includes/db.php');
include('includes/functions.php');

function isUnique($email){
	$query = "select * from benutzer_larima where email='$email'";
	global $db;
	
	$result = $db->query($query);
	
	if($result->num_rows > 0){
		return false;
	}
	else return true;	
	
}

if(loggedIn()){
	header("Location:myaccount.php");
	exit();
}

if(isset($_POST['register'])){
	$_SESSION['name'] = $_POST['name'];
	$_SESSION['email'] = $_POST['email'];
	$_SESSION['pass'] = $_POST['pass'];
	$_SESSION['confirm_pass'] = $_POST['confirm_pass'];
	
    if(strlen($_POST['name'])<3){
    	header("Location:register.php?err=" . urlencode("The name must be at least 3 characters long"));
		exit();
    }
	else if($_POST['pass'] != $_POST['confirm_pass']){
		header("Location:register.php?err=" . urlencode("The password and confirm password do dot match."));
		exit();
	}
	else if(strlen($_POST['pass'])<5){
    	header("Location:register.php?err=" . urlencode("The password must be at least 5 characters long"));
		exit();
	}
	else if(strlen($_POST['confirm_pass'])<5){
    	header("Location:register.php?err=" . urlencode("The password must be at least 5 characters long"));
		exit();
	}
	else if(!isUnique($_POST['email'])){
		header("Location:register.php?err=" .  urlencode("The email is already taken."));
		exit();
	}
	else {
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$passwort = mysqli_real_escape_string($db, $_POST['pass']);
		$pass = hash('ripemd160', $passwort);
		$token = bin2hex(openssl_random_pseudo_bytes(32));
		$status = 0;
		
		$query = "insert into benutzer_larima (name, email, pass, token, status) values('$name', '$email', '$pass', '$token', '$status')";
		
		$db->query($query);
		$message = "Hi $name! Account created, here is the activation link http://localhost:8888/Zeiterfassung/www/activate.php?token=$token";
		mail($email, 'Activate Account', $message, "From: juan.cuartas@gmail.com");
		header("Location:index.php?success=" . urlencode("Activation mail sent!"));
		exit();
	}
}


?>