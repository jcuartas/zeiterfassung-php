<?php
/*
 * Following code will get single user details
 * A user is identified by product email (email) and password(pw)
 */
  //Declares that this app generates a JSON type
header('Content-type: application/json');
//Opens access to the connections which require this application.
header("Access-Control-Allow-Origin: *");

// array for JSON response
$response = array();

//session_start();
include('includes/config.php');
include('includes/db.php');
include('includes/functions.php');

if(isset($_POST['fid'])){
	if($_POST['fid'] == "login"){ 
		if(isset($_POST['login'])){
			$email = mysqli_real_escape_string($db, $_POST['email']);
			$pass = mysqli_real_escape_string($db, $_POST['pass']);
			$pass = hash('ripemd160', $pass);
	
			$query = "select * from benutzer_larima where email = '$email' and pass = '$pass'";
	
			$result = $db->query($query);
	
			if($row = $result->fetch_assoc()){
				//User Exists!!!!
				
				if($row['status'] == 1){
					// User is activated. He can enter the system.
					$response["success"] = 1;
					$response["message"] = "User correctly logged in";
					$response["u_session"] = $row["pk"];
					$response["user_name"] = $row["email"];
					
				}else {
					// User is not activated, error.
					$response["success"] = 0;
					$response["message"] = "Das Benutzerkonto ist nicht aktiviert!";
				}	
				
			}else {
				// Wrong Username / Password. Error.
				$response["success"] = 0;
				$response["message"] = "Falscher Benutzername / Passwort!";
			}
		}
	}
	else if($_POST['fid'] == "register") {

		function isUnique($email){
			$query = "select * from benutzer_larima where email='$email'";
			//global $db;
			
			$result = $db->query($query);
			
			if($result->num_rows > 0){
				return false;
			}
			else return true;	
			
		}

		if(isset($_POST['register'])){
				
			if(strlen($_POST['name'])<3){
				$response["success"] = 0;
				$response["message"] = "The name must be at least 3 characters long";
			}
			else if($_POST['pass'] != $_POST['confirm_pass']){
				$response["success"] = 0;
				$response["message"] = "The password and confirm password do dot match";
			}
			else if(strlen($_POST['pass'])<5){
				$response["success"] = 0;
				$response["message"] = "The password must be at least 5 characters long";
			}
			else if(strlen($_POST['confirm_pass'])<5){
				$response["success"] = 0;
				$response["message"] = "The password must be at least 5 characters long";
			}
			else if(!isUnique($_POST['email'])){
				$response["success"] = 0;
				$response["message"] = "The email is already taken";
			}
			else {
				$name = mysqli_real_escape_string($db, $_POST['name']);
				$email = mysqli_real_escape_string($db, $_POST['email']);
				$passwort = mysqli_real_escape_string($db, $_POST['pass']);
				$pass = hash('ripemd160', $passwort);
				$token = bin2hex(openssl_random_pseudo_bytes(32));
				$status = 0;
				
				$query = "insert into benutzer_larima (name, email, pass, token, status) values('$name', '$email', '$pass', '$token', '$status')";
				
				$db->query($query);
				$message = "Hi $name! Account created, here is the activation link http://localhost:8888/Zeiterfassung/www/activate.php?token=$token";
				mail($email, 'Activate Account', $message, "From: juan.cuartas@gmail.com");
				$response["success"] = 1;
				$response["message"] = "Activation mail sent!";
			}
		}
	}
	//else if($_POST['fid']== "etwas") {}
}
// echoing JSON response
echo json_encode($response);
?>