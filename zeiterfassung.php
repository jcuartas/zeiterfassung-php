<?php
/*
 * Following code will get single user details
 * A user is identified by product email (email) and password(pw)
 */
  //Declares that this app generates a JSON type
header('Content-type: application/json');
//Opens access to the connections which require this application.
header("Access-Control-Allow-Origin: *");

// array for JSON response
$response = array();

//session_start();
include('includes/config.php');
include('includes/db.php');
include('includes/functions.php');


if(isset($_POST['u_session'])){
    $pk = $_POST['u_session'];
}
if(isset($_POST['pk_proj'])){
    $pk_proj = $_POST['pk_proj'];
}

if(isset($_POST['qry'])){
    $postqry = $_POST['qry'];
    if($postqry == "benpro"){
        // Alle Projekte
        
        $query = "select * from SichtBenutzerProjekt where pkbenutzer = '$pk'";
        
        $result = $db->query($query);
        $response["allep"] = array();
        while ($row = $result->fetch_assoc()){
            $allep = array();
            $allep["pkprojekt"]=$row["pkprojekt"];
            $allep["color"]=$row["color"];
            $allep["nummer"]=$row["Nummer"];
            $allep["titel"]=$row["Titel"];
            array_push($response["allep"], $allep);
        }
        $response["success"] = 1;
        
        
    } else if($postqry == "zeiterfassung") {
        
        $query = "select * from zeiterfassung_larima where PKBenutzer = '$pk'";
        
        $result = $db->query($query);
        $response["zeiterf"] = array();
        
        while($row = $result-fetch_assoc()){
            $zeiterf = array();
            $zeiterf[""] = $row[""];
        }
        $response["success"] = 1;
    }
    else if($postqry == "benlaufpro") {
        $query = "SELECT pkze, PKProjekt, StartZeit, Nummer, Titel FROM SichtBenutzerLaufProjekt where PKBenutzer = '$pk'";

        $result = $db->query($query);
        $response["laufproj"] = array();
        while ($row = $result->fetch_assoc()){
            $laufp = array();
            $laufp["pkze"]=$row["pkze"];
            $laufp["pkprojekt"]=$row["PKProjekt"];
            $laufp["startzeit"]=$row["StartZeit"];
            $laufp["nummer"]=$row["Nummer"];
            $laufp["titel"]=$row["Titel"];
            array_push($response["laufproj"], $laufp);
        }
        $response["success"] = 1;
    }
    else if ($postqry == "zeiterfassung") {
        $query = "SELECT pk, StartZeit, EndZeit, Bemerkung FROM 	zeiterfassung_larima WHERE PKProjekt = 7080 and PKBenutzer = '$pk' order by StartZeit";
    }
    else if ($postqry == "pause"){
        $query = "SELECT pk, StartZeit, EndZeit, PKBenutzer, 'Status', Heruntergeladen FROM pause_larima WHERE PKBenutzer = '$pk'";
        $result = $db->query($query);
        $response["pause"] = array();
        while ($row = $result->fetch_assoc()){
            $pause = array();
            $pause["pk"]=$row["pk"];
            $pause["startzeit"]=$row["StartZeit"];
            $pause["endzeit"]=$row["EndZeit"];
            $pause["status"]=$row["Status"];
            array_push($response["pause"], $pause);
        }
        $response["success"] = 1;
    }
    else if ($postqry == "projekt") {
        $query = "SELECT pk, Nummer, Titel FROM projekte_larima WHERE pk = '$pk_proj'";
        $result = $db->query($query);
        $row = $result->fetch_assoc();
        $response["projekt"]= $row["Titel"];
        $response["success"]=1;
    }
}

/*

$query = "select * from SichtBenutzerProjekt";

$result = $db->query($query);
$response["lauf"] = array();
while($row = $result->fetch_assoc()){
    $lauf = array();
    $lauf["pkprojekt"]=$row["pkprojekt"];
    $lauf["color"]=$row["color"];
    $lauf["nummer"]=$row["Nummer"];
    $lauf["titel"]=$row["Titel"];
    array_push($response["lauf"], $lauf);
}
$response["success"]=1;
*/

// echoing JSON response
echo json_encode($response);
?>